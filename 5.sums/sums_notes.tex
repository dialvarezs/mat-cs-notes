%!TeX program = lualatex

\documentclass[11pt]{article}
\usepackage[spanish]{babel}
\usepackage{fontspec}
\usepackage[left=2cm,right=2cm,top=3cm,bottom=3cm]{geometry}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{booktabs}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{parskip}
\usepackage{calc}
\usepackage{cancel}
\usepackage{multicol}
\usepackage{tikz}

\pagestyle{fancy}
\fancyhf{}
\rhead{\scshape Diego Alvarez S. -- Nov, 2020}
\lhead{\scshape Sumas}
\cfoot{\thepage/\pageref{LastPage}}

\begin{document}
\setlength{\abovedisplayskip}{1ex}

\begin{center}
    \huge
    \textbf{Sumas}
\end{center}
\vspace{1em}

Una suma se refiere al resultado que se obtiene al sumar una serie de elementos, que se puede expresar de la siguiente manera:
\[
    a_{1} + a_{2} + a_{3} + ... + a_{n}
\]
Donde cada $a_{k}$ está definido de alguna manera. La notación con los puntos suspensivos ($...$) puede parecer la más natural, sin embargo posee limitaciones en lo que respecta a la forma en que se definen los elementos (al igual que con la declaración de conjuntos por extensión). Por esta razón, existe un operador que permite expresarlas mediante una fórmula que es evaluada para cada elemento:
\[
    \sum_{k=1}^{n} k = 1 + 2 + 3 + ... + n
\]

De esta forma, cada elemento queda definido en función de la variable $i$ que itera entre los límites que que se indican bajo y sobre el símbolo de sumatoria (ambos inclusive). Se puede encontrar esta notación con algunas variaciones en libros y material asociado, pero acá se usará de esta manera.

Encontrar el resultado la suma de una serie hasta su enésimo elemento se vuelve un resultado útil, ya que presenta una fórmula generalizada que se puede utilizar con cualquier número finito. Por ejemplo, la suma presentada previamente tiene como resultado:
\[
    S = \sum_{k=1}^{n} k = \frac{n(n+1)}{2}
\]
Encontrar la solución de las sumas generalmente requiere utilizar adecuadamente las propiedades de éstas y en algunos el uso de ``trucos'' para manipularlas de forma conveniente. Se puede llegar al resultado anterior de la siguiente manera :
\begin{enumerate}
    \item Consideremos dos veces la suma, pero la segunda en orden inverso:
          \begin{alignat*}{13}
              S \quad = \quad 1 & \; + \; & 2     & \; + \; & 3     & \; + \; & ... & \; + \; & (n-2) & \; + \; & (n-1) & \; + \; & n \\
              S \quad = \quad n & \; + \; & (n-1) & \; + \; & (n-2) & \; + \; & ... & \; + \; & 3     & \; + \; & 2     & \; + \; & 1
          \end{alignat*}
    \item Al sumar los elementos como están alineados, cada par de ellos da como resultado $n+1$, y hay $n$ elementos, con lo que se puede deducir la fórmula indicada:
          \begin{align*}
              2S & = n(n+1)           \\
              S  & = \frac{n(n+1)}{2}
          \end{align*}
\end{enumerate}

\subsection*{Propiedades de las Sumas}
\begin{align*}
    \sum_{k=m}^{n} a_{k} + \sum_{k=m}^{n} b_{k} & = \sum_{k=m}^{n} (a_{k} + b_{k}) \\
    c\sum_{k=m}^{n} a_{k}                       & = \sum_{k=m}^{n} ca_{k}
\end{align*}

\subsection*{Convergencia de una Serie}
Con los ejemplos vistos, es fácil reemplazar $n$ por un número específico y tendríamos la suma de una serie finita, pero ¿qué sucede si hacemos que $n$ sea $\infty$? En ese caso habrá que hacer un análisis de límites, el cual puede dar dos resultados posibles:
\begin{itemize}
    \item que el resultado sea inifinito igualmente, y en tal caso se dice que la serie diverge
    \item que la suma se acerque cada vez más a un valor en particular, entonces la suma converge a ese valor
\end{itemize}

\subsection*{Series Geométricas}
Las series geométricas son las que pueden ser expresadas de la siguiente manera:
\[
    \sum_{k=0}^{n-1}ar^{k} = a + ar + ar^{2} + ... + ar^{n-1}
\]
Se puede llegar a al resultado de su sumatoria utilizando una segunda serie multiplicada por $r$:
\begin{alignat*}{13}
    S \quad         & = \quad a & \; + \; & ar & \; + \; & ar^{2} & \; + \; & ... & \; + \;   & ar^{n-1} &                  \\
    r \cdot S \quad & = \quad   & \; + \; & ar & \; + \; & ar^{2} & \; + \; & ... & ~ \; + \; & ar^{n-1} & \; + \; & ar^{n}
\end{alignat*}
Al restar estas dos series se cancelarán la mayoría de los elementos:
\begin{align*}
    S - rS   & = a - ar^{n}                                                 \\
    S(1 - r) & = a(1 - r^{n})                                               \\
    S        & = a\left(\frac{1 - r^{n}}{1 - r}\right) \quad (si\ r \neq 1)
\end{align*}

\subsection*{Series Telescópicas}
Una serie telescópica es aquella en la que sus elementos se van cancelando hasta dejar un número fijo de ellos. Ejemplo:
\[
    \sum_{k=1}^{n} \frac{1}{k(k+1)}
\]
Se puede descomponer el término en una resta, de la siguiente manera:
\[
    \frac{1}{k(k+1)} = \frac{1+k-k}{k(k+1)} = \frac{(k+1)-k}{k(k+1)} = \frac{\cancel{k+1}}{k\cancel{(k+1)}} - \frac{\cancel{k}}{\cancel{k}(k+1)} = \frac{1}{k} - \frac{1}{k+1}
\]
Luego, al desarollarla, se puede ver claramente que los elementos se van cancelando consecutivamente:
\begin{align*}
    \sum_{k=1}^{n} \frac{1}{k} - \frac{1}{k+1} & = \left(1 - \frac{1}{2}\right) + \left(\frac{1}{2} - \frac{1}{3}\right) + \left(\frac{1}{3} - \frac{1}{4}\right) + ... + \left(\frac{1}{n} - \frac{1}{n+1}\right) \\
                                               & = 1 + \left(-\frac{1}{2} + \frac{1}{2}\right) + \left(-\frac{1}{3} + \frac{1}{3}\right) + ... + \left(-\frac{1}{n} + \frac{1}{n}\right) - \frac{1}{n+1}           \\
                                               & = 1 - \frac{1}{n+1}
\end{align*}

\subsection*{Números Armónicos}
En ocasiones podrá aparecer la suma $\sum_{k=1}^{n} \frac{1}{k}$, la cual corresponde al enésimo número armónico. No hay una expresión cerrada conocida para los números armónicos, pero se han logrado buenas aproximaciones. Formalmente,
\[
    H_{n} ::= \sum_{k=1}^{n} \frac{1}{n}
\]
Este valor puede utilizarse como parte de la solución al resolver sumas. Por ejemplo, en la siguiente:
\[
    \sum_{k=0}^{n} \frac{k+1}{k+2}
\]
Lo primero será manipular un poco la fórmula para eliminar la suma en el numerador:
\[
    \sum_{k=0}^{n} \frac{k+1}{k+2} = \sum_{k=0}^{n} \frac{k+2-1}{k+2} = \sum_{k=0}^{n} \frac{\cancel{k+2}}{\cancel{k+2}} - \frac{1}{k+2} = \sum_{k=0}^{n} 1 - \sum_{k=0}^{n} \frac{1}{k+2}
\]
El resultado de la primera suma es $n+1$ (ya que empieza desde 0), y la segunda se parece un poco a la fórmula de los números armónicos, pero aún no lo suficiente. Se pueden alterar los límites para obtener una expresión más parecida, sin embargo hay que tener en cuenta que esta debe comenzar en 1 siempre, lo cual requiere otro paso adicional:
\[
    \sum_{k=0}^{n} \frac{1}{k+2} = \sum_{k=2}^{n+2} \frac{1}{k} = \sum_{k=2}^{n+2} \frac{1}{k} + 1 - 1 = \sum_{k=1}^{n+2} \frac{1}{k} - 1 = H_{n+2} - 1
\]
Uniendo todo lo ya resuelto:
\[
    \sum_{k=0}^{n} \frac{k+1}{k+2} = n + 1 - (H_{n+2} - 1) = - H_{n+2} + n + 2
\]


\newpage
\subsection*{Ejercicios Propuestos}

\begin{enumerate}
    \item Escriba las siguientes series en notación sumatoria ($\sum$):
          \begin{enumerate}
              \item $4^2 + 5^2 + 6^2 + ... + (n+2)^2$
              \item $\frac{1}{3} + \frac{2}{5} + \frac{3}{7} + \frac{4}{9} + ... + \frac{20}{41} $
              \item $1 - \frac{1}{4} + \frac{1}{9} - \frac{1}{16} + \frac{1}{25} - \frac{1}{36}$
              \item $3x^2 + 6x^4 + 9x^6 + 12x^8 + ... + 36x^{24}$
          \end{enumerate}

    \item Resuelva las siguientes sumas:
          \begin{multicols}{4}
              \begin{enumerate}
                  \everymath{\displaystyle}
                  \item $\sum_{k=1}^{137} k$
                  \item $\sum_{k=51}^{100} k^{2}$
                  \item $\sum_{k=1}^{n} k^2 - (k + 1)^2$
                  \item $\sum_{k=1}^{n} \frac{k+7}{3}$
                  \item $\sum_{k=1}^{n} (k+2)^{2}$
                  \item $\sum_{k=1}^{n} k3^{k}$
                  \item $\sum_{k=1}^{n} k2^{-k}$
                  \item $\sum_{k=1}^{2n} \frac{2k-1}{2k}$
                  \item $\sum_{k=1}^{n} \frac{k+1}{k}$
                  \item $\sum_{k=1}^{n} \frac{k+1}{k+2}$
                  \item $\sum_{k=1}^{n} \frac{2k+1}{k(k+1)}$
                  \item $\sum_{k=1}^{n} \frac{1}{k^2+3k+2}$
                  \item $\sum_{k=1}^{n} \frac{(-4)^{3k}}{5^{k-1}}$
                  \item $\sum_{k=1}^{n} \frac{1}{4k^2+4k}$
                  \item $\sum_{k=7}^{n+2} 3nx^{2n}$
                  \item[]
              \end{enumerate}
          \end{multicols}
\end{enumerate}

\end{document}