COMPILER       = latexmk -lualatex -silent
SOURCE_FILES   = $(shell find . -type f -name '*.tex')
PDF_FILES      = $(subst .tex,.pdf,$(SOURCE_FILES))
TMP_EXTENSIONS = aux|log|bak|fls|fdb_latexmk|synctex\.gz


all: $(PDF_FILES)

%.pdf: %.tex
	latexindent -s -w -l indentconfig.yaml $<
	$(COMPILER) -outdir=$(dir $<) $<

clean:
	find -regextype posix-extended -regex ".*\.($(TMP_EXTENSIONS))" -exec rm -f {} +

rm: clean
	find -name '*.pdf' -exec rm -f {} +