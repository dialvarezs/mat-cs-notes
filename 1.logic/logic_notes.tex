%!TeX program = lualatex

\documentclass[11pt]{article}
\usepackage[spanish]{babel}
\usepackage{fontspec}
\usepackage[left=2cm,right=2cm,top=3cm,bottom=3cm]{geometry}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{booktabs}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{parskip}
\usepackage{calc}
\usepackage{enumitem}
\usepackage{multicol}

\pagestyle{fancy}
\fancyhf{}
\rhead{\scshape Diego Alvarez S. -- Oct, 2020}
\lhead{\scshape Lógica Proposicional}
\cfoot{\thepage/\pageref{LastPage}}

\setlength{\headheight}{14pt}
\setlength{\abovedisplayskip}{1ex}


\begin{document}

\begin{center}
    \huge
    \textbf{Lógica Proposicional}
\end{center}
\vspace{1em}

La lógica es una forma de pensar que nos permite deducir nueva información basándose en información ya conocida de una forma razonable y objetiva.

Una proposición matemática (o expresión lógica) consiste en una frase respecto a la cual se puede decir sin lugar a dudas si es verdadera o falsa. Por ejemplo, las siguientes son expresiones lógicas:
\begin{itemize}
    \item Hoy está soleado
    \item 2 + 1 = 3
    \item 55 es un cuadrado perfecto
\end{itemize}

Para construir expresiones más complejas (o compuestas) se pueden utilizar conectores lógicos (como \textbf{\emph{y}}, \textbf{\emph{o}}, y \textbf{\emph{no}}), donde cada expresión que la compone se puede evaluar lógicamente (verdadera o falsa) y el valor lógico de la expresión completa se obtendrá en función de estos valores y los conectores lógicos.

Ejemplos:
\begin{itemize}
    \item 20 es divisible por 5 \textit{\textbf{y}} 10 es divisible por 2
    \item \emph{\textbf{Si}} 3 es primo \emph{\textbf{entonces}} (9 es par \emph{\textbf{o}} 14 es impar)
\end{itemize}

Para generalizar y simplificar su expresión podemos utilizar variables para cada declaración.
\begin{center}
    $P:$ La raíz cuadrada de 8 es 2. \\
    $Q:$ La raíz cuadrada de 9 es 3. \\
    $R:$ $P$ o $Q$
\end{center}

\subsection*{Conectores Lógicos}

\begin{minipage}[t]{.32\linewidth}
    \begin{center}
        \normalsize\bfseries Disyunción
    \end{center}
    La operación \emph{\textbf{o}}/\emph{\textbf{OR}} es verdadera cuando cualquier variable en la expresión es verdadera.

    \vspace{1ex}
    \begin{center}
        \begin{tabular}{c c | c}
            $P$ & $Q$ & $P \vee Q$ \\ \hline
            V   & V   & V          \\
            V   & F   & V          \\
            F   & V   & V          \\
            F   & F   & F          \\
        \end{tabular}
    \end{center}
\end{minipage}
\hfill
\begin{minipage}[t]{.32\linewidth}
    \begin{center}
        \normalsize\bfseries Conjunción
    \end{center}
    La operación \emph{\textbf{y}}/\emph{\textbf{AND}} es verdadera sólo cuando todas las variables en la expresión son verdaderas.

    \vspace{1ex}
    \begin{center}
        \begin{tabular}{c c | c}
            $P$ & $Q$ & $P \wedge Q$ \\ \hline
            V   & V   & V            \\
            V   & F   & F            \\
            F   & V   & F            \\
            F   & F   & F            \\
        \end{tabular}
    \end{center}
\end{minipage}
\hfill
\begin{minipage}[t]{.32\linewidth}
    \begin{center}
        \normalsize\bfseries Negación
    \end{center}
    La operación \emph{\textbf{no}}/\emph{\textbf{NOT}} invierte el valor lógico de una variable. Puede usarse la notación $\neg P$ o $\overline{P}$.

    \vspace{1ex}
    \begin{center}
        \begin{tabular}{c | c}
            $P$ & $\neg P$ \\ \hline
            V   & F        \\
            F   & V        \\
        \end{tabular}
    \end{center}
\end{minipage}

\vspace{2ex}
\begin{minipage}[t]{.5\linewidth - .5em}
    \begin{center}
        \normalsize\bfseries Implicación
    \end{center}
    Decimos que $P$ \emph{\textbf{implica}} $Q$ y escribimos $P \Rightarrow Q$ cuando la expresión $\neg P \vee Q$ es verdadera. En ocasiones también puede expresarse como \emph{\textbf{si}} $P$ \emph{\textbf{entonces}} $Q$. El lado izquierdo de la implicación ($P$ en este caso) es llamada la premisa y decimos que $P$ es suficiente para probar $Q$ (llamada conclusión).

    \vspace{1ex}
    \begin{center}
        \begin{tabular}{c c | c}
            $P$ & $Q$ & $P \Rightarrow Q$ \\ \hline
            V   & V   & V                 \\
            V   & F   & F                 \\
            F   & V   & V                 \\
            F   & F   & V                 \\
        \end{tabular}
    \end{center}
\end{minipage}
\hfill
\begin{minipage}[t]{.5\linewidth -.5em}
    \begin{center}
        \normalsize\bfseries Sí y solo sí
    \end{center}
    Decimos $P$ \emph{\textbf{sí y sólo sí}} $Q$ y escribimos $P \Leftrightarrow Q$ cuando $(P \Rightarrow Q) \wedge (Q \Rightarrow P)$ es verdadero, que corresponde al hecho de que ambas variables tengan el mismo valor de verdad.

    \vspace{1ex}
    \begin{center}
        \begin{tabular}{c c | c}
            $P$ & $Q$ & $P \Leftrightarrow Q$ \\ \hline
            V   & V   & V                     \\
            V   & F   & F                     \\
            F   & V   & F                     \\
            F   & F   & V                     \\
        \end{tabular}
    \end{center}
\end{minipage}

Existen otras operaciones lógicas que pueden ser reproducidas a través de las ya presentadas. Algunos ejemplos son \emph{\textbf{XOR}}, \emph{\textbf{NAND}}, \emph{\textbf{NOR}} y \emph{\textbf{XNOR}}.

Todas las operaciones lógicas puedes utilizarse de manera conjunta. Se pueden utilizar paréntesis para hacer definir concretamente el orden de evaluación. Si no hay paréntesis, las operaciones son evaluadas de izquierda a derecha con las siguientes prioridades:

\begin{center}
    \begin{tabular}{c c}
        Prioridad & Operación                      \\ \midrule
        1         & $\neg$                         \\
        2         & $\wedge, \vee$                 \\
        3         & $\Rightarrow, \Leftrightarrow$
    \end{tabular}
\end{center}

\subsubsection*{Tablas de Verdad}
A evaluar una expresión considerando todas las posibles combinaciones de valores para las variables involucradas, se puede formar lo que se denomina ``tabla de verdad''. La tabla de verdad de una expresión tendrá $2^{n}$ entradas, donde $n$ es el número de variables asociadas a la expresión. Por ejemplo, para la expresión
\[
    A: p \vee (q \Rightarrow r)
\]
Tendríamos la siguiente tabla de verdad
\begin{center}
    \begin{tabular}{c c c | c}
        $p$ & $q$ & $r$ & $p \vee (q \Rightarrow r)$ \\ \hline
        V   & V   & V   & V                          \\
        V   & V   & F   & V                          \\
        V   & F   & V   & V                          \\
        V   & F   & F   & V                          \\
        F   & V   & V   & V                          \\
        F   & V   & F   & F                          \\
        F   & F   & V   & V                          \\
        F   & F   & F   & V                          \\
    \end{tabular}
\end{center}

Las tablas de verdad nos permiten verificar de manera práctica el valor lógico de una expresión, al evaluar todos los casos posibles. Dependiendo la complejidad de la expresión, puede ser conveniente añadir columnas con expresiones parciales, de forma de simplificar las evaluaciones.


\subsection*{Cuantificadores}

Los cuantificadores son utilizados para indicar el alcance de una expresión explicitando los elementos a los que sería aplicable la expresión lógica.
\begin{itemize}
    \item Cuantificador universal: se utiliza para indicar que la expresión es válida para todos los objetos a los cuales puede ser aplicado. Para utilizar este cuantificador se utiliza el símbolo $\forall$ (``para todo''). Es posible definir el conjunto de objetos en la propia expresión. Por ejemplo:
          \[
              \forall x \in \mathbb{R}.\, x^{2} \geq 0
          \]

    \item Cuantificar existencial: se utiliza para indicar la existencia de al menos un elemento particular para el cual la expresión es válida. Se utiliza el símbolo $\exists$ (``existe un(a)''). Se puede utilizar junto a un signo de exclamación ($\exists!$) para acotar la cuantificación a un único elemento. Ejemplo:

          Sea $D$ el conjunto de los números pares y $P(x):$ $x$ es primo, entonces
          \[
              \exists! x \in D.\, P(x)
          \]
          es una expresión verdadera ya que 2 es el único número primo par.

\end{itemize}

Los cuantificadores pueden ser negados para definir negativamente el conjunto para la expresión, así como también pueden utilizarse múltiples veces en una misma expresión.

Cuando se desea negar una expresión que tiene un cuantificador, hay que tener en cuenta basta con considerar el caso mínimo para negar la expresión en lugar de aplicar la negación directamente. Por ejemplo, si tenemos la expresión "todas las fichas en esta bolsa a son blancas", la cual puede ser expresada como (siendo $A$ la bolsa, y $b(x)$ una función que indica que la ficha es blanca):
\[
    P: \forall x \in A, b(x)
\]
Intuitivamente se podría pensar en añadir la negación a toda la expresión, con lo que la expresión sería "todas las fichas de esta bolsa no son blancas", sin embargo, sólo hace falta que una ficha no sea blanca para que la expresión $P$ ya no sea verdadera, por lo que en realidad la forma adecuada sería:
\[
    \neg (\forall x \in A, b(x)) \Leftrightarrow \exists x \in A, \neg b(x)
\]


\subsection*{Equivalencia}

Una expresión es llamada una tautología cuando es siempre verdadera, independiente de los valores de sus variables. Asimismo, una expresión es llamada una contradicción cuando su valor es siempre falso. En caso de no ser una contradicción (lo que no necesariamente quiere decir que sea una tautología), se dice que la expresión es satisfacible (o consistente).

Dos expresiones son equivalentes si tienen la misma tabla de verdad. La equivalencia se denota con el símbolo $\equiv$. En general, diremos que $P$ y $Q$ son equivalentes (y escribiremos $P \equiv Q$) si $P \Leftrightarrow Q$ es una tautología.

La forma más directa de probar la equivalencia de dos expresiones es a través de su tabla de verdad, pero este puede ser un trabajo extenso dependiendo la cantidad de variables involucradas. Sin embargo, se puede hacer uso de equivalencias lógicas y propiedades para transformar una expresión hasta una forma que resulte conveniente para su comparación.

\subsubsection*{Equivalencias Lógicas}

\begin{minipage}[t]{.45\linewidth}
    \centering{\bfseries Identidades}
    \begin{align*}
        p \wedge V \equiv p \\
        p \vee F \equiv p
    \end{align*}

    \centering{\bfseries Leyes de Dominación}
    \begin{align*}
        p \vee V \equiv V \\
        p \wedge F \equiv F
    \end{align*}

    \centering{\bfseries Leyes de Idempotencia}
    \begin{align*}
        p \vee p \equiv p \\
        p \wedge p \equiv p
    \end{align*}

    \centering{\bfseries Doble Negación}
    \begin{align*}
        \neg (\neg p) \equiv p
    \end{align*}
\end{minipage}
\hfill
\begin{minipage}[t]{.45\linewidth}
    \centering{\bfseries Leyes de De Morgan}
    \begin{align*}
        \neg (p \wedge q) \equiv \neg p \vee \neg q \\
        \neg (p \vee q) \equiv \neg p \wedge \neg q
    \end{align*}

    \centering{\bfseries Leyes de Asociatividad}
    \begin{align*}
        (p \vee q) \vee r \equiv p \vee (q \vee r) \\
        (p \wedge q) \wedge r \equiv p \wedge (q \wedge r)
    \end{align*}

    \centering{\bfseries Leyes de Distributividad}
    \begin{align*}
        p \vee (q \wedge r) \equiv (p \vee q) \wedge (p \vee r) \\
        p \wedge (q \vee r) \equiv (p \wedge q) \vee (p \wedge r)
    \end{align*}

    \centering{\bfseries Leyes de Absorción}
    \begin{align*}
        p \vee (p \wedge q) \equiv p \\
        p \wedge (p \vee q) \equiv p
    \end{align*}
\end{minipage}

\newpage
\subsection*{Ejercicios Propuestos}

\begin{enumerate}
    \item Considere las siguientes proposiciones:
          \begin{enumerate}[label=\Roman*.]
              \item Quienes programan en Python también programan en JavaScript.
              \item Quienes programan en C igual programan en Java.
              \item Quienes no programan en JavaScript tampoco programan en Java.
              \item Quienes programan en Rust no programan en JavaScript.
          \end{enumerate}
          Si todas las proposiciones son verdaderas, ¿cuáles de las siguientes proposiciones son verdaderas?
          \begin{enumerate}
              \item Quienes programan en C también programan en JavaScript.
              \item Quienes programan en Python también programan en Java.
              \item Quienes programan en JavaScript no programan en C.
              \item Quienes programan en C no programan en Python.
              \item Quienes programan en Python programan en C.
              \item Quienes programan Rust no programan en C.
          \end{enumerate}

    \item Encuentre el valor de verdad de las siguientes expresiones considerando $p=F$, $q=V$, $r=V$:
          \begin{multicols}{2}
              \begin{enumerate}
                  \item $(p \Rightarrow \neg q) \vee \neg (r \wedge q)$
                  \item $(\neg p \vee \neg q) \Rightarrow (p \vee \neg r)$
                  \item $\neg (\neg p \Rightarrow \neg q) \wedge r$
                  \item $\neg(\neg p \Rightarrow q \wedge \neg r)$
              \end{enumerate}
          \end{multicols}

    \item Utilice la tabla de verdad para verificar si las siguientes expresiones son consistentes, tautologías o contradicciones:
          \begin{multicols}{2}
              \begin{enumerate}
                  \item $((p \wedge q) \wedge \neg q) \wedge (q \vee p) \Rightarrow \neg p$
                  \item $(p \Rightarrow q) \wedge \neg q \Rightarrow \neg p$
                  \item $(p \Rightarrow q) \Rightarrow (p \Rightarrow \neg q)$
                  \item $(p \vee q \Rightarrow r) \vee p \vee q$
                  \item $(p \vee q) \wedge (p \Rightarrow r \wedge q) \wedge (q \Rightarrow \neg r \wedge p)$\vee
                  \item $(p \Rightarrow (q \Rightarrow r)) \Rightarrow ((p \Rightarrow q) \Rightarrow (p \Rightarrow r))$
                  \item $(p \vee q) \wedge (\neg q \wedge \neg p)$
                  \item $(\neg p \Rightarrow q) \vee((p \wedge \neg r) \Leftrightarrow q)$
                  \item $(p \Rightarrow q) \wedge (p \Rightarrow \neg q)$
                  \item $(p \Rightarrow (q \vee r)) \vee (r \Rightarrow \neg p)$
              \end{enumerate}
          \end{multicols}

    \item Formalice las siguientes expresiones y verifique si son correctas:
          \begin{enumerate}
              \item Si Luigi gana la la carrera, entonces Mario llega segundo o Yoshi llega tercero. Yoshi no llegó tercero. Por lo tanto, si Mario no llegó segundo, entonces Luigi no ganó la carrera.

              \item Si Luigi gana la la carrera, entonces Mario llega segundo y Yoshi tercero. Mario no llegó segundo. Por lo tanto, Luigi no ganó la carrera.

              \item Si Luigi gana la carrera, entonces si Mario llega segundo Yoshi llega tercero. Mario no llegó segundo. Por lo tanto, o Luigi ganó o Yoshi llegó tercero.

              \item Si juegas y estudias aprobarás el ramo, pero si juegas y no estudias no aprobarás. En consecuencia, si juegas, entonces estudias y apruebas, o no estudias y no apruebas.
          \end{enumerate}

    \item Verifique si las siguientes equivalencias son correctas:
          \begin{multicols}{2}
              \begin{enumerate}
                  \item $p \Leftrightarrow q \equiv (\neg p \vee q) \wedge (\neg q \vee p)$
                  \item $(p \vee q) \wedge (\neg p \Rightarrow \neg q) \equiv q$
                  \item $\neg (p \wedge q) \equiv \neg p \vee \neg q$
                  \item $((p \Rightarrow q) \Rightarrow q) \Rightarrow q \equiv p \Rightarrow q$
              \end{enumerate}
          \end{multicols}



    \item La tabla de verdad de la operación \emph{\textbf{XOR}} (or exclusivo) es la siguiente:
          \vspace{1ex}
          \begin{center}
              \begin{tabular}{c c | c}
                  $P$ & $Q$ & $P \oplus Q$ \\ \hline
                  V   & V   & F            \\
                  V   & F   & V            \\
                  F   & V   & V            \\
                  F   & F   & F            \\
              \end{tabular}
          \end{center}
          Escriba una expresión lógica equivalente a esta operación utilizando \emph{\textbf{AND}}, \emph{\textbf{OR}} y \emph{\textbf{NOT}}.

    \item Pruebe cualquiera de las leyes de absorción sin utilizar tablas de verdad. Justifique cada paso.

    \item Usted se encuentra jugando un juego de acción y exploración, y llegado un punto sólo le queda el 5\% de su vida y no tiene más pociones, por lo que es poco probable llegar hasta el próximo punto de guardado y ya se encuentra demasiado lejos del último. Detrás de una puerta secreta encuentra un acertijo que le permite escoger un cofre de tres disponibles. Hay un cofre que recuperará su vida al 100\%, le dará un incremento de defensa temporal y 5 pociones, mientras que los otros explotarán y le harán morir (perdiendo así la mitad de la experiencia ganada desde el último punto de guardado). Cada cofre tiene una inscripción a modo de pista sobre su contenido; las cuales son:
          \begin{description}
              \item[Cofre 1] Las pociones no están aquí
              \item[Cofre 2] Este cofre explotará
              \item[Cofre 3] Las pociones están en el Cofre 2
          \end{description}
          Sólo un mensaje es cierto, los otros dos son falsos. ¿Cuál caja escogería? Formalice el acertijo en lógica proposicional y encuentre la solución usando una tabla de verdad.

    \item Link fue en búsqueda de la Espada Maestra al Bosque Kolog,y al llegar se encuentra con 3 Kologs que le bloquean en paso para ponerlo a prueba. La prueba consiste en adivinar si cada uno de ellos está diciendo la verdad o está mintiendo. Cada uno dice una frase:
          \begin{description}
              \item[Kolog 1] Si estoy mintiendo, entonces exactamente dos de nosotros están diciendo la verdad
              \item[Kolog 2] El Kolog 1 está mintiendo
              \item[Kolog 3] Todos estamos mintiendo, o al menos uno de nosotros está diciendo la verdad
          \end{description}
          ¿Cuáles dicen la verdad y cuáles mienten?
\end{enumerate}

\end{document}