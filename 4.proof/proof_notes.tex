%!TeX program = lualatex

\documentclass[11pt]{article}
\usepackage[spanish]{babel}
\usepackage{fontspec}
\usepackage[left=2cm,right=2cm,top=3cm,bottom=3cm]{geometry}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{booktabs}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{parskip}
\usepackage{calc}
\usepackage{enumitem}
\usepackage{multicol}
\usepackage[makeroom]{cancel}

\pagestyle{fancy}
\fancyhf{}
\rhead{\scshape Diego Alvarez S. -- Nov, 2020}
\lhead{\scshape Demostración Matemática}
\cfoot{\thepage/\pageref{LastPage}}

\setlength{\headheight}{14pt}
\setlength{\abovedisplayskip}{1ex}

\begin{document}

\begin{center}
    \huge
    \textbf{Demostración Matemática}
\end{center}
\vspace{1em}

La demostración matemática consiste en comprobar la veracidad de una proposición utilizando una secuencia de deducciones lógicas desde axiomas o declaraciones previamente probadas.


\subsection*{Deducciones Lógicas}

Las deducciones lógicas o reglas de inferencia son usadas para probar nuevas proposiciones utilizando otras ya probadas. Una regla de inferencia fundamental es \textit{modus ponens}. La regla dice que la prueba de $P$ junto a la prueba de $P \Rightarrow Q$ es una prueba de $Q$:
\[
    (P \wedge (P \Rightarrow Q)) \Rightarrow Q
\]
Otras reglas comunes son las siguientes:
\begin{align*}
    (\neg Q \wedge (P \Rightarrow Q))            & \Rightarrow \neg P            \\
    ((P \vee Q) \wedge \neg P)                   & \Rightarrow Q                 \\
    ((P \Rightarrow Q) \wedge (Q \Rightarrow R)) & \Rightarrow (P \Rightarrow R) \\
    (\neg P \Rightarrow \neg Q)                  & \Rightarrow (Q \Rightarrow P)
\end{align*}


\subsection*{Modelos de Demostración}

Para reducir la complejidad de formular demostraciones, existen distintos modelos estándar, que pueden seguirse como una guía de pasos para demostrar una proposición. La posibilidad de utilizar uno u otro dependerá de la proposición a demostrar, por lo que será conveniente ver algunos ejemplos reconocibles.

\subsubsection*{Demostración Directa}

Es la forma más básica de demostración. Consiste en una cadena de inferencia que deriva un resultado de otro, donde cada paso depende solamente de pasos previos o de teoremas, axiomas, definiciones, etc.

\textbf{Ejemplo}

Se demostrará que la suma de dos números racionales es un número racional. En el ejemplo se numerará cada uno de los pasos para ilustrar mejor el método, pero no es necesario.

Se definirá lo siguiente:
\begin{itemize}
    \item \textbf{Axioma 1}: La adición, sustracción y multiplicación son operaciones cerradas en los números enteros. Formalmente, si $x, y \in \mathbb{Z}$, entonces $x + y \in \mathbb{Z}$, $x -y \in \mathbb{Z}$, y $xy \in \mathbb{Z}$.
    \item \textbf{Axioma 2}: Sean $x, y \in \mathbb{R}$ tal que $x \neq 0$ y $y \neq 0$, entonces $xy \neq 0$.
    \item \textbf{Definición 1}: Un número $x$ es racional si existen dos enteros $p$ y $q$, con $q \neq 0$, tal que $x = p/q$.
\end{itemize}

Entonces, la demostración sería la siguiente:
\begin{enumerate}
    \item Sean $x, y \in \mathbb{Q}$. Necesitamos demostrar que $x + y \in \mathbb{Q}$.
    \item Como $x \in \mathbb{Q}$, entonces existen dos enteros $a, b$ con $b \neq 0$, tal que $x = a/b$ (Definición 1).
    \item Como $y \in \mathbb{Q}$, entonces existen dos enteros $c, d$ con $d \neq 0$, tal que $y = c/d$ (Definición 1).
    \item Desarrollando $x +y$ obtenemos,
          \[
              x + y = \frac{a}{b} + \frac{c}{d} = \frac{ad + bc}{bd}
          \]
    \item Como la suma y la multiplicación son cerradas en los enteros (Axioma 1), entonces $p = ad + bc \in \mathbb{Z}$.
    \item Como la multiplicación es cerradas en los enteros (Axioma 1), entonces $q = bd \in \mathbb{Z}$.
    \item Por el Axioma 2, $q \neq 0$.
    \item Por sustitución, $x + y = p / q$, donde $p, q \in \mathbb{Z}$ y $q \neq 0$. Por la Definición 1, $x + y \in \mathbb{Q}$, que lo que se buscaba probar.
\end{enumerate}


\subsubsection*{Demostración por Contraposición}

La demostración por contraposición se sustenta en la equivalencia lógica entre $p \Rightarrow q$ y $\neg q \Rightarrow \neg p$. El método consiste en transformar la expresión a demostrar en su forma contrapuesta y demostrar esa en su lugar (la cual debería ser una demostración más sencilla). Una vez demostrada la expresión contrapuesta, como ambas son lógicamente equivalente, se habrá demostrado la original igualmente.

\textbf{Ejemplo}

Demostrar que: con $n > 0$, si $4^{n} - 1$ es primo, entonces $n$ es impar.

No se puede ver una forma clara de hacer la demostración directamente, por lo que se puede probar por contraposición. Lo primero que se debe hacer es ``transformar'' la expresión original en su forma contrapuesta (es decir, pasar de $p \Rightarrow q$ a $\neg q \Rightarrow \neg p$). En este caso, la expresión quedaría de la siguiente forma:

Con $n > 0$, si $n$ es par, entonces $4^{n} - 1$ no es primo.

Dado que $n$ es par, se puede hacer el reemplazo $n = 2k, k \in \mathbb{Z}$. Y al hacer este reemplazo se podrá observar que se obtiene la forma de una suma por diferencia:
\[
    4^{n} - 1 = 4^{2k} - 1 = (4^{k} + 1)(4^{k} - 1)
\]

Y como el número ha podido ser factorizado, este no puede ser un número primo (como $k > 0$, entonces ninguno de los factores puede ser 1). De esta forma queda demostrada la expresión contrapuesta, y por consiguiente también la expresión original.


\subsubsection*{Demostración por Casos}

En ocasiones las expresiones a demostrar considerarán múltiples casos. En estas ocasiones, se analizará cada caso de forma independiente, lo cual es llamado demostración por casos. Es importante cubrir todas las posibilidades, ya que un ejemplo no es una demostración.

\newpage
\textbf{Ejemplo}

Demostrar que $x + |x - 7| \geq 7$ con $x \in \mathbb{R}$.

Se sabe que el valor absoluto de un número $x$ podrá tener dos casos posibles por su definición (que $x$ sea positivo y que $x$ sea negativo).
En este caso, como el valor absoluto está aplicado a $x - 7$, entonces el punto de corte en ambos casos será 7 (ya que es el valor para el cual la expresión se vuelve 0).
\begin{itemize}
    \item \textbf{Caso 1}: Asumir que $x \geq 7$

          Entonces $x - 7 \geq 0$ y $|x - 7| = x - 7$, y utilizando la expresión original:
          \begin{align*}
              x + (x - 7) & \geq 7 \\
              2x - 7      & \geq 7 \\
              2(7) - 7    & \geq 7 \\
              7           & \geq 7
          \end{align*}

    \item \textbf{Caso 1}: Asumir que $x < 7$

          Entonces $x - 7 < 0$ y $|x - 7| = -(x - 7) = 7 - x$, y utilizando la expresión original:
          \begin{align*}
              x + (7 - x) & \geq 7 \\
              x - x + 7   & \geq 7 \\
              7           & \geq 7
          \end{align*}
\end{itemize}

En ambos casos la desigualdad se cumplió, y por tanto se ha demostrado que $x + |x - 7| \geq 7$.

\subsubsection*{Demostración por Contradicción}

Para probar una expresión por contradicción se asume que la expresión es falsa, y entonces se muestra que suposición lleva a algo ilógico. Entonces se concluye que es incorrecto asumir que la expresión es falsa, por lo tanto debe ser verdadera.

\textbf{Ejemplo}

Probar que $1 + \sqrt{2}$ es irracional.

Para probar por contradicción se va a suponer lo contrario, es decir, que $1 + \sqrt{2}$ es racional, y por la definición de números racionales existen dos enteros sin factores comunes $m$ y $n$ tal que $1 + \sqrt{2} = \frac{m}{n}$. Entonces
\begin{align*}
    1 + \sqrt{2} & = \frac{m}{n}     \\
    \sqrt{2}     & = \frac{m}{n} - 1 \\
    \sqrt{2}     & = \frac{m-n}{n}   \\
    \sqrt{2} n   & = m - n           \\
    2 n^{2}      & = (m - n)^{2}
\end{align*}
Dado que $2n^2$ es par, entonces $(m - n)^2$ igual y por tanto $(m - n)$ también. Las únicas formas posibles de obtener un número par de esa resta es que ambos sean pares o ambos sean impares, y no pueden ser ambos pares ya que no tienen factores comunes, lo cual quiere decir que se puede expresar $n$ como $2q + 1$. También se reemplazará $(m - n)$ por $2k$ (un número par) para simplificar las operaciones. Entonces
\begin{align*}
    2 (2q + 1)^{2}    & = (2k)^{2} \\
    2 (4q^2 + 4q + 1) & = 4k^2     \\
    4q^2 + 4q + 1     & = 2k^2
\end{align*}
Habiendo llegado a este punto, se tiene a la izquierda de la ecuación un número impar, mientras que a la derecha uno par, lo cual es una contradicción. Por lo tanto, la suposición inicial es falsa, y entonces queda demostrado que $1 + \sqrt{2}$ es un número irracional.


\subsubsection*{Demostración por Inducción}

La demostración por inducción sirve para probar que una expresión es verdadera para todos los positivos enteros. Dada, una expresión $P(x)$, para llevar a cabo el proceso de demostración se debe hacer lo siguiente:
\begin{enumerate}
    \item Probar $P(1)$ (llamado paso base, puede ser otro entero también)
    \item Asumir que $P(k)$ es verdadera (llamado la hipótesis inductiva)
    \item Probar que $P(k) \Rightarrow P(k+1)$ (llamado el paso inductivo)
\end{enumerate}

\textbf{Ejemplo}

Probar que para $n \in \mathbb{Z}^+, n \geq 2$
\[
    \prod_{i = 2}^{n} \left( 1 - \frac{1}{i^2} \right) = \left( 1 - \frac{1}{2^2} \right) \cdot \left( 1 - \frac{1}{3^2} \right) \cdot ... \cdot \left( 1 - \frac{1}{n^2} \right) = \frac{n+1}{2n}
\]
\textbf{Paso base:} Se puede verificar que la expresión es verdadera para $n = 2$:
\begin{align*}
    1 - \frac{1}{2^2} & = \frac{2 + 1}{2 \cdot 2} \\
    \frac{3}{4}       & = \frac{3}{4}
\end{align*}

\textbf{Paso inductivo:} Sea $k \geq 2$, supongamos que la expresión es verdadera para $n = k$ (hipótesis inductiva). Entonces
\begin{align*}
    \prod_{i = 2}^{k + 1} \left( 1 - \frac{1}{i^2} \right) & = \left( \frac{k+1}{2k} \right) \cdot \left( 1 - \frac{1}{(k + 1)^2} \right) \\
                                                           & = \frac{\cancel{k + 1}}{2k} \cdot \frac{(k+1)^2 - 1}{(k+1)^{\cancel{2}}}     \\
                                                           & = \frac{(k^2 + 2k + 1) - 1}{2k(k+1)}                                         \\
                                                           & = \frac{\cancel{k}(k + 2)}{2\cancel{k}(k+1)}                                 \\
                                                           & = \frac{k + 2}{2(k+1)}
\end{align*}

Si se evalúa la fórmula en $n = k + 1$, se llega al mismo resultado obtenido en el paso inductivo, por lo tanto, la expresión es verdadera para $n \in \mathbb{Z}^+$ con $n \geq 2$.



\subsubsection*{Refutación por Contra-ejemplo}

La refutación por contra-ejemplo es útil para refutar expresiones que tienen cuantificación universal. Se basa en la idea de que un único contra ejemplo basta para refutar una expresión, es decir que si tenemos $(\forall x)P(x)$, y se encuentra un número $a$ tal que $\neg P(a)$, entonces se habrá demostrado que $(\exists a)(\neg P(a))$, y por tanto $P(x)$ no se cumple para todos los $x$ posibles.

\newpage
\subsection*{Ejercicios Propuestos}

\begin{enumerate}
    \item Demostrar directamente:
          \begin{enumerate}
              \item La suma de dos números impares es un número par.
              \item Si $x$ e $y$ son dos enteros positivos tal que $x$ es divisor de $y$, entonces $x \leq y$.
              \item Si $x \in \mathbb{Q}$, entonces $5x^{2} -12x + 17 \in \mathbb{Q}$.
          \end{enumerate}

    \item Demostrar por casos:
          \begin{enumerate}
              \item Si $n$ es un entero, entonces $3n^{2} + n + 10$ es par.
              \item Si $x$ es un número real, entonces $-5 \leq |x + 2| - |x - 3| \leq 5$.
              \item Mostrar que $k^5 - k$ es divisible por 3 si $k \in \mathbb{Z}^{+}$. Pista: factorizar y considerar los posibles restos cuando $k$ es divido por 3.
          \end{enumerate}

    \item Demostrar por contradicción:
          \begin{enumerate}
              \item Sea $a \in \mathbb{Z}$ y $p$ cualquier primo divisor de $a$. Entonces $p$ no es un divisor de $a+1$.
              \item Si $a, b \in \mathbb{Z}$, entonces $a^2 - 4b - 3 \neq 0$.
              \item Sean $a, b, c \in \mathbb{Z}$. Si $a^2 + b^2 = c^2$, entonces $a$ es par o $b$ es par.
              \item Hay infinitos números primos.
              \item No hay soluciones enteras positivas para la ecuación $x^2 - y^2 = 1$.
              \item $\sqrt{3}$ es irracional.
          \end{enumerate}

    \item Demostrar por inducción:
          \begin{enumerate}
              \item Para cualquier $x \in \mathbb{R}|x \neq 1$ y $n \in \mathbb{Z}|n \geq 0$, se cumple que
                    \[
                        1 + x + x^2 + ... + x^n = \frac{1 - x^{n+1}}{1 - x}
                    \]
              \item Para cualquier $n \in \mathbb{Z}^{+}$, se cumple que
                    \[
                        \frac{1}{1 \cdot 2} + \frac{1}{2 \cdot 3} + ... + \frac{1}{n(n+1)} = \frac{n}{n+1}
                    \]
              \item Para cualquier $n \in \mathbb{Z}^{+}$, se cumple que
                    \[
                        1^{3} + 2^{3} + 3^{3} + ... + n^{3} = \frac{n^2(n+1)^2}{4}
                    \]
              \item Para cualquier $n \in \mathbb{Z}^{+}$, se cumple que
                    \[
                        1 \cdot 3 + 2 \cdot 4 + 3 \cdot 5 + ... + n(n+2) = \frac{n(n+1)(2n+7)}{6}
                    \]

              \item Para cualquier $n \in \mathbb{Z}^{+}$, se cumple que $5^{2n} - 1$ es divisible por 24.
          \end{enumerate}
\end{enumerate}

\end{document}