%!TeX program = lualatex

\documentclass[11pt]{article}
\usepackage[spanish]{babel}
\usepackage{fontspec}
\usepackage[left=2cm,right=2cm,top=3cm,bottom=3cm]{geometry}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{booktabs}
\usepackage{fancyhdr}
\usepackage{lastpage}
\usepackage{parskip}
\usepackage{calc}
\usepackage{multicol}
\usepackage{venndiagram}
\usepackage{tikz}

\pagestyle{fancy}
\fancyhf{}
\rhead{\scshape Diego Alvarez S. -- Nov, 2020}
\lhead{\scshape Conjuntos}
\cfoot{\thepage/\pageref{LastPage}}

\setlength{\headheight}{14pt}
\setlength{\abovedisplayskip}{1ex}

\begin{document}

\begin{center}
    \huge
    \textbf{Conjuntos}
\end{center}
\vspace{1em}

Un conjunto es una colección numerable de elementos diferentes. En un conjunto no es relevante el orden de sus elementos, sólo los propios elementos en sí, por lo que se puede hacer pensar en un conjunto como una bolsa donde hay un grupo de elementos distinguibles. Se puede definir de la siguiente manera:
\[
    S = \{ 1, 2, 3, 4 \}
\]
Si se puede observar un patrón en los elementos se puede utilizar la elipsis:
\[
    R = \{ 1, 2, 3, ..., 100 \}
\]
Los elementos de un conjunto igual pueden ser otros conjuntos:
\[
    T = \{ \{ a, b \}, \{ a, c \}, \{ c, d \}\}
\]

\subsection*{Pertenencia}

Cuando se quiere indicar que un elemento pertenece a un conjunto, se utiliza la notación $y \in P$, lo que quiere decir que $y$ se encuentra en el conjunto P. Por el contrario, cuando se quiere indicar que un elemento no pertenece a un conjunto, se utiliza la notación $z \notin P$.

\subsection*{Igualdad}

Se dice que los conjunto $A$ y $B$ son iguales, y se denota $A = B$, si
\[
    (\forall x)((x \in A) \Leftrightarrow (x \in B))
\]
Por otro lado, se dice que $A$ y $B$ no son iguales, y se denota $A \neq B$, si
\[
    (\exists x \in B)(x \notin A) \vee (\exists x \in A)(x \notin B)
\]

\subsection*{Conjuntos Comunes}

\begin{itemize}
    \item $\mathbb{Z}$: El conjunto de los enteros.
    \item $\mathbb{Z}^+$: El conjunto de los enteros positivos.
    \item $\mathbb{N}$: El conjunto de los números naturales.
    \item $\mathbb{Q}$: El conjunto de los números racionales.
          Un número $x$ es racional si existen dos enteros $a, b$ con $b \neq 0$, tal que $x = a/b$.
          Si no existen dichos enteros, entonces $x$ es irracional.
    \item $\mathbb{R}$: El conjunto de los números reales, que son los racionales e irracionales.
    \item $\mathbb{R}^+$: El conjunto los reales positivos.
    \item $\mathbb{C}$: El conjunto los números complejos.
\end{itemize}

\subsection*{Cardinalidad}

La cardinalidad es la cantidad de elementos que posee un conjunto. Normalmente se denota con dos barras verticales ($|A|$), aunque también puede utilizarse $n(A)$, $card(A)$ o $\#A$.
Un conjunto que tiene cardinalidad 1 es llamado conjunto unitario. Por otra parte, se dice que un conjunto es enumerable si se puede asignar un positivo entero a cada uno de sus elementos. Algunos ejemplos de estos son $\mathbb{N}$, $\mathbb{Z}$ y $\mathbb{Q}$.

\subsection*{Declaración por Comprensión}

En ocasiones será conveniente definir una regla para determinar los elementos que pertenecen a un conjunto, para lo cual se usa la declaración por comprensión.
Se denota de la siguiente manera
\[
    S = \{ x|P(x) \}
\]
y se entiende como ``$S$ el conjunto de todos los elementos que tiene la propiedad $P(x)$''.
Es posible indicar un conjunto al cual pertenece $x$. Por ejemplo, para el conjunto de todos los enteros impares mayores a 17:
\[
    S = \{ x \in \mathbb{Z}|(x \bmod 2 = 1) \wedge (x > 17) \}
\]

\subsection*{Subconjuntos}
Si un conjunto $A$ está completamente contenido en un conjunto $B$, se dice que $A$ es un subconjunto de $B$ y se denota $A \subseteq B$. Formalmente,
\[
    A \subseteq B \equiv ((\forall x \in A)(x \in B))
\]
Si además $A$ y $B$ no son iguales, se dice que $A$ es un subconjunto propio de $B$, lo cual se denota
\[
    A \subset B \equiv ((A \subseteq B) \wedge (A \neq B))
\]
Nótese que
\[
    (A = B) \Rightarrow (A \subseteq B \wedge B \subseteq A)
\]

\subsection*{Operaciones}

\begin{minipage}[t]{.5\linewidth - 1em}
    \begin{center}
        \normalsize\bfseries Intersección
    \end{center}

    La intersección de dos conjuntos $A$ y $B$, denotada $A \cap B$, es el conjunto que contiene sólo los elementos que están en ambos conjuntos.
    \[
        A \cap B = \{ x | (x \in A) \wedge (x \in B) \}
    \]
    Sea $A_0, A_1, ..., A_n$ una secuencia numerable (posiblemente infinita) de conjuntos, entonces
    \begin{align*}
        \bigcap_{i=0}^{n} A_i & = \{ x | (x \in A_{i})(\forall i) \}                                   \\
                              & = \{ x|(x \in A_0) \wedge (x \in A_1) \wedge ... \wedge (x \in A_n) \}
    \end{align*}

    \begin{center}
        \begin{venndiagram2sets}
            \fillACapB
        \end{venndiagram2sets}
    \end{center}
\end{minipage}
\hfill
\begin{minipage}[t]{.5\linewidth - 1em}
    \begin{center}
        \normalsize\bfseries Unión
    \end{center}

    La unión de dos conjuntos $A$ y $B$, denotada $A \cup B$, es el conjunto que contiene los elementos que se encuentran en $A$ o en $B$.
    \[
        A \cup B = \{ x | (x \in A) \vee (x \in B) \}
    \]
    Sea $A_0, A_1, ..., A_n$ una secuencia numerable (posiblemente infinita) de conjuntos, entonces
    \begin{align*}
        \bigcup_{i=0}^{n} A_i & = \{ x | (\exists i)(x \in A_{i}) \}                             \\
                              & = \{ x|(x \in A_0) \vee (x \in A_1) \vee ... \vee (x \in A_n) \}
    \end{align*}

    \begin{center}
        \begin{venndiagram2sets}
            \fillA \fillB
        \end{venndiagram2sets}
    \end{center}
\end{minipage}

\begin{minipage}[t]{.5\linewidth - 1em}
    \begin{center}
        \normalsize\bfseries Complemento
    \end{center}

    El complemento de un conjunto $A$, denotado $\overline{A}$ o $A^{C}$, consiste en todos los conjuntos en el dominio (conjunto universo) que no están en $A$.
    \[
        \overline{A} = \{ x \in U | x \notin A \}
    \]

    \begin{center}
        \begin{tikzpicture}
            \filldraw[fill=lightgray] (-2.02,-1.7) rectangle (3.02,1.7);
            \fill[white] (0.6,0) circle (1.2);
            \draw (0.6,0) circle (1.2) (0.6, 1.15) node [text=black,below] {$A$};
        \end{tikzpicture}
    \end{center}
\end{minipage}
\hfill
\begin{minipage}[t]{.5\linewidth - 1em}
    \begin{center}
        \normalsize\bfseries Diferencia
    \end{center}

    La diferencia de A y B es la parte de $A$ que no está en $B$, y es denotado $A - B$ (o $A \backslash B$).
    También puede ser llamado complemento relativo de $B$ en $A$, especialmente cuando $B$ es subconjunto de $A$.
    \[
        A - B = A \cap \overline{B} = \{ x | (x \in A) \wedge (x \notin B) \}
    \]

    \begin{center}
        \begin{venndiagram2sets}
            \fillOnlyA
        \end{venndiagram2sets}
    \end{center}
\end{minipage}

\subsection*{Conjunto Potencia}
Sea $S$ un conjunto, entonces el conjunto potencia de $S$, denotado $\mathcal{P}(S)$, el el conjunto que posee todos los subconjuntos de $S$.
\begin{align*}
    S    & = \{ 1, 2, 3 \}                                                                                 \\
    P(S) & = \{ \emptyset, \{ 1 \}, \{ 2 \}, \{ 3 \}, \{ 1, 2 \}, \{ 1, 3 \}, \{ 2, 3 \}, \{ 1, 2, 3 \} \}
\end{align*}

\subsection*{Equivalencias de Conjuntos}
En las siguientes expresiones $S$, $T$ y $W$ son cualquier conjunto, $\emptyset$ es el conjunto vacío, y $\mathbf{U}$ es el conjunto universal.

\begin{minipage}[t]{.45\linewidth}
    \centering{\bfseries Identidades}
    \begin{align*}
        S \cup \emptyset  & = S \\
        p \cap \mathbf{U} & = S
    \end{align*}

    \centering{\bfseries Leyes de Dominación}
    \begin{align*}
        S \cap \emptyset  & = \emptyset  \\
        S \cup \mathbf{U} & = \mathbf{U}
    \end{align*}

    \centering{\bfseries Leyes de Idempotencia}
    \begin{align*}
        S \cup S & = S \\
        S \cap S & = S
    \end{align*}

    \centering{\bfseries Doble Complemento}
    \begin{align*}
        \overline{(\overline{S})} = S
    \end{align*}
\end{minipage}
\hfill
\begin{minipage}[t]{.45\linewidth}
    \centering{\bfseries Leyes de De Morgan}
    \begin{align*}
        \overline{S \cap T} & = \overline{S} \cup \overline{T} \\
        \overline{S \cup T} & = \overline{S} \cap \overline{T}
    \end{align*}

    \centering{\bfseries Leyes de Asociatividad}
    \begin{align*}
        S \cup (T \cup W) & = (S \cup T) \cup W \\
        S \cap (T \cap W) & = (S \cap T) \cap W
    \end{align*}

    \centering{\bfseries Leyes de Distributividad}
    \begin{align*}
        S \cup (T \cap W) & = (S \cup T) \cap (S \cup W) \\
        S \cap (T \cup W) & = (S \cap T) \cup (S \cap W)
    \end{align*}

    \centering{\bfseries Leyes de Absorción}
    \begin{align*}
        S \cup (S \cap T) & = S \\
        S \cap (S \cup T) & = S
    \end{align*}
\end{minipage}

\newpage
\subsection*{Ejercicios Propuestos}

\begin{enumerate}
    \item Sea $T = \{ x \in \mathbb{N}|7x^2 = 63 \}$. ¿Es $T = 3$? ¿Por qué?

    \item Sea $M = \{ a, b, c, d \}$. Indique si las siguientes afirmaciones son correctas:
          \begin{multicols}{4}
              \begin{enumerate}
                  \item $e \in M$
                  \item $a \subset M$
                  \item $\{ b, d \} \in M$
                  \item $\{ b \} \subset M$
                  \item $\{ a \} \in P(M)$
                  \item $\{ a, b, c, d \} \subseteq M$
                  \item $\emptyset \in M$
              \end{enumerate}
          \end{multicols}

    \item Asumiendo que $A$, $B$ y $C$ son conjuntos, indique en un diagrama de Venn la región que correspondería al conjunto $X = \overline{((\overline{A \cap B}) \cap C)} \cap (\overline{A} \cap (B \cup C))$.

    \item Describa por extensión los siguientes conjuntos:
          \begin{multicols}{2}
              \begin{enumerate}
                  \item $\{ x | x \text{ es una cifra del número 2014201} \}$
                  \item $\{ x \in \mathbb{R} | 2x - 3 = 0 \}$
                  \item $\{ x \in \mathbb{Z} | x^2 = 9 \}$
                  \item $\{ x \in \mathbb{Q} | x^2 = 3 \}$
              \end{enumerate}
          \end{multicols}

    \item ¿Cuál es el conjunto potencia de $\{ \emptyset \}$?

    \item Sea $S = \{ k^2 | k \in \mathbb{Z}^{+} \}$. ¿Es la cardinalidad de $S$ mayor, menor, o igual a la cardinalidad de $\mathbb{Z}^{+}$? Justifique apropiadamente.

    \item Sean $A$ y $B$ dos subconjuntos del conjunto universal $\mathbf{U}$ que tiene $N$ elementos.\\
          Si $|A \cap B| = \dfrac{2}{5}N$, $|B = \dfrac{1}{2}N|$ y $\left|\overline{\overline{A} \cap \overline{B}}\right| = \dfrac{3}{20}N$, calcule
          \begin{enumerate}
              \item $|A|$
              \item $|(A-B)\cup(B-A)|$
          \end{enumerate}

    \item Sean $A=\left\{ x \in \mathbb{R} \bigg| \dfrac{x+1}{x-1} \geq 2 \right\}$ y $B = \{ x \in \mathbb{R} | x^2 + 4x + 3 < 0 \}$.\\
          Exprese mediante intervalos dichos conjuntos y calcule la unión, la intersección y la diferencia de uno con el otro (ambos casos).
          Calcule además los conjuntos complementarios de cada uno y compruebe que se cumplan las leyes de De Morgan.

    \item Se encuesta a 100 personas obteniéndose la siguiente información:
          \begin{itemize}
              \item Todos los encuestados que poseen una bicicleta también poseen audífonos inalámbricos
              \item 54 de los encuestados son jóvenes
              \item 30 de los encuestados que son jóvenes no son propietarios de una bicicleta
              \item 5 de los encuestados no jóvenes sólamente poseen audífonos inalámbricos
              \item 15 de los encuestados que son poseen audífonos inalámbricos no poseen bicicleta
          \end{itemize}
          \begin{enumerate}
              \item Hacer un diagrama adecuado a la situación indicando la cardinalidad de cada región
              \item ¿Cuántos encuestados que son jóvenes sólamente son propietarios de una bicicleta?
              \item ¿Cuántos encuestados no jóvenes no poseen audífonos inalámbricos?
          \end{enumerate}
\end{enumerate}

\end{document}